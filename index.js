// Express package
const express = require("express");
// Mongoose Package
const mongoose = require("mongoose");

const app = express();
const port = 3003;

// Connect to mongodb atlas
mongoose.connect(
  "mongodb+srv://admin123:admin123@zuitt-bootcamp.cnhvgta.mongodb.net/",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.listen(port, () => console.log(`Server running at port ${port}`));
